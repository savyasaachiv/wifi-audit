import subprocess

def get_connected_clients(wifi_name):
    try:
        # Run 'netsh wlan show network' to get interface name
        interface_info = subprocess.check_output(['netsh', 'wlan', 'show', 'network']).decode('utf-8')
        interface_lines = interface_info.split('\n')

        # Find the interface name corresponding to the specified Wi-Fi name
        interface_name = None
        for line in interface_lines:
            if wifi_name in line:
                interface_name = line.split(':')[1].strip()
                break

        if interface_name:
            # Run 'netsh wlan show hostednetwork' to get connected clients
            client_info = subprocess.check_output(['netsh', 'wlan', 'show', 'hostednetwork']).decode('utf-8')
            client_lines = client_info.split('\n')

            connected_clients = []
            for line in client_lines:
                if 'SSID' in line and interface_name in line:
                    connected_clients.append(line.split(':')[1].strip())
            return connected_clients
        else:
            return "Wi-Fi network not found."

    except subprocess.CalledProcessError as e:
        return "Error:", e

# Example usage:
wifi_name = input("Enter the Wi-Fi name: ")
connected_clients = get_connected_clients(wifi_name)
print("Connected clients to", wifi_name + ":", connected_clients)
