import subprocess
import re

def get_available_wifi_full():
    result = subprocess.run(['netsh', 'wlan', 'show', 'network'], capture_output=True, text=True)
    networks_info = re.findall(r'(SSID\s+\d+.?Signal.)', result.stdout, re.DOTALL)
    networks = []
    for info in networks_info:
        ssid_match = re.search(r'SSID\s+\d+\s+:\s(.+)', info)
        signal_match = re.search(r'Signal\s+:\s(.+)', info)
        if ssid_match and signal_match:
            ssid = ssid_match.group(1)
            signal = signal_match.group(1)
            networks.append((ssid, signal))
    return networks

print(get_available_wifi_full())
