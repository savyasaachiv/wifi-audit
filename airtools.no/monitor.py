import subprocess

def set_monitor_mode(interface):
    subprocess.run(['sudo', 'ifconfig', interface, 'down'])
    subprocess.run(['sudo', 'iwconfig', interface, 'mode', 'monitor'])
    subprocess.run(['sudo', 'ifconfig', interface, 'up'])

def set_managed_mode(interface):
    subprocess.run(['sudo', 'ifconfig', interface, 'down'])
    subprocess.run(['sudo', 'iwconfig', interface, 'mode', 'managed'])
    subprocess.run(['sudo', 'ifconfig', interface, 'up'])

if __name__ == "__main__":
    interface = input("Enter the name of the interface (e.g., wlan0): ")

    print("Setting interface to monitor mode...")
    set_monitor_mode(interface)
    print("Interface set to monitor mode successfully.")
    input('wait')

    print("Setting interface to managed mode...")
    set_managed_mode(interface)
    print("Interface set to managed mode successfully.")
