from airtools import apscan, monman
from sys import argv
from os.path import exists

#print(argv)
BSSID=''
MAC=''
CHANNEL=''
if exists('interface'):
    interface=open('interface').read().strip()
else:
    interface=input('Please enter interface to use ')
    open('interface','w').write(interface)

if len(argv)==1:
    print('''
    Utility to perform or advanced and specific scans

    start.sh -t "parameter-type argument"

    eg:
    start.sh -t "bssid,wifi-name"  # scans for all access points which have the provided wifi-name
    start.sh -t "channel, channel-number"  # scans for all access points running under specified channel
    start.sh -t "mac, mac-address"  # scans for access points that match the provided mac address

    Multiple arguments can be clubed together
    start.sh -t "bssid,homeWifi,Office" "channel,7,10,11"
          ''')
    exit(0)

for _ in argv[1:]:
    if ',' in _:
        params=_.split(',')
        if params[0]=='bssid':
            BSSID+='\n'.join(params[1:])
        elif params[0]=='mac':
            MAC+='\n'.join(params[1:])
        elif params[0]=='channel':
            CHANNEL+='\n'.join(params[1:])
        else:
            print(f'{params[0]} is not a valid argument!')
            exit(0)
    else:
        print('Incorrect type format')
        exit(0)

probLine='Probing all access points having '

if BSSID!='':
    probLine+='BSSID "'+BSSID.replace('\n',' ')+'" '
if MAC!='':
    probLine+='MAC "'+MAC.replace('\n',' ')+'" '
if CHANNEL!='':
    probLine+='CHANNEL "'+CHANNEL.replace('\n',' ')+'" '

print(probLine)

if not monman.monitor():
    print('Unable to switch to monitor mode')
    exit(0)

aps=apscan.scan(interface)
results=[]

for _ in aps:
    if aps[_][4] in CHANNEL.split('\n'):
        apss=aps[_]
        apss.append(_)
        results.append(apss)
    elif aps[_][0] in BSSID.split('\n'):
        apss=aps[_]
        apss.append(_)
        results.append(apss)
    elif _ in MAC:
        apss=aps[_]
        apss.append(_)
        results.append(apss)

print('\n')
for _ in results:
    print(','.join(_))
print('\n')
