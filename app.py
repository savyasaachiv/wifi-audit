from flask import Flask, render_template, request
from threading import Thread
from airtools import monman, deauth, apscan, apclient

debug=0
interface=open('interface').read().strip()

#do airmon and if successful only then proceed with starting web server
if  not monman.monitor():
    print('Unable to switch to monitor mode')
    exit(0)
if debug:
    print('DEBUGGING ENABLED')

###################################################################################################
# PRE DEFINED VARS
ApScanhtml='Please run access point scan first'
ClientScanhtml='Please run client scan first'
macAdd=''


app=Flask(__name__)


###################################################################################################
@app.route('/') # WORKING
def index_page():
    return render_template('index.html')



@app.route('/AP') # WORKING
def discoverScan():
    #ap scan
    return render_template('AP.html')



@app.route('/APC', methods=['GET','POST']) # WORKING
def discoverScanClients():
    if request.method=='GET':
        #ap scan with clients
        return render_template('APC.html')

    if request.method=='POST':
        global macAdd
        global ClientScanhtml

        macAdd=request.form['mac']
        channel=request.form['channel']

        apclients=apclient.scan(interface, channel, macAdd, debug)
        apcresults=macAdd+'\n\n'

        ClientScanhtml='''
        <p class="montserrat-regular small"><a href="http://0.0.0.0:8000/AP_Client_Results.csv">Download scan results</a></div>
        <table>
        <thead>
        <tr>
        <th>Clients</th>
        </tr>
        </thead>
        <tbody>
        '''

        for _ in apclients:
            apcresults+=f'{_}\n'
            ClientScanhtml+=f'<tr><td> {_} </td></tr>'
        ClientScanhtml+='</tbody></table>'

        open('downloads/AP_Client_Results.csv','w').write(apcresults)

        return render_template('APC.html',clients=ClientScanhtml)



@app.route('/DA', methods=['GET','POST']) # 
def deauthAttacks(): #WORKING
    #deauthentication attacks
    if request.method=='GET':
        return render_template('DA.html')
    if request.method=='POST':
        client=request.form['Client']
        bssid=request.form['Bssid']
        frames=request.form['frames']

        if (client.strip()=='') or (bssid.strip()==''):
            return render_template('DA.html',message='Please fill both the fields with valid data')

        Thread(target=deauth.run_deauth, args=[interface,bssid,client,frames]).start()

        return render_template('DA.html',message=f'Sending {frames} deauthentication frames to {client} under {bssid}')



#api end points to send scan results
@app.route('/AP_scan_results') # WORKING
def apScanResults():
    global ApScanhtml

    aps=apscan.scan(interface, debug=debug)
    # aps format
    # { "wifi-mac": ["bssid","privacy","cipher","authentication","channel"] }

    ApScanhtml= '''
    <p class="montserrat-regular small"><a href="http://0.0.0.0:8000/AP_Scan_Results.csv">Download scan results</a></div>
    <table>
    <thead>
    <tr> 
    <th>Mac Address</th> <th>Bssid</th> <th>Privacy</th> <th>Cipher</th> <th>Authentication</th> <th>Channel</th>
    </tr> 
    </thead>
    <tbody> 
    '''

    apresults='Mac Address, Bssid, Privacy, Cipher, Authetication, Channel\n'

    for _ in aps:
        apresults+=f'{_},{aps[_][0]},{aps[_][1]},{aps[_][2]},{aps[_][3]},{aps[_][4]}\n'
        ApScanhtml+=f'<tr><td>{_}</td><td>{aps[_][0]}</td><td>{aps[_][1]}</td><td>{aps[_][2]}</td><td>{aps[_][3]}</td><td>{aps[_][4]}</td></tr>'
    ApScanhtml+='</tbody></table>'

    open('downloads/AP_Scan_Results.csv','w').write(apresults)

    return ApScanhtml



@app.route('/AP_prev_scan_results') # WORKING
def apPrevScanRes():
    global ApScanhtml
    return ApScanhtml



@app.route('/APC_prev_scan')
def apcPrevScanres():
    global macAdd
    global ClientScanhtml
    return ClientScanhtml.replace('Clients',macAdd)




