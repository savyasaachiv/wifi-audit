#!/bin/bash

sudo flask &> /dev/null
if [ "$?" != "0" ]; then
    echo Please install flask to run this program
    exit
fi
ls interface &> /dev/null
if [ "$?" != "0" ]; then
    echo Please enter interface to use
    read interface
    echo $interface > interface
fi

sleep 3 && xdg-open "http://0.0.0.0:5000" &
cd downloads
python3 -m http.server &
httpvar=$!
cd ..
sudo flask run
kill $httpvar
