A program to automate wifi audit and scanning and perform filter based functions.

# Installation
Clone the repo using the following command
```
git clone https://gitlab.com/savyasaachiv/wifi-audit.git --depth 1

# install requirements
pip install flask
```

# Requirements
- Python3
- Python-Flask
- Aircrack-ng
- Unix Operating system
- Web browser

# Run
To run the program, start the web server by running `start.sh`
```
chmod +x start.sh
./start.sh
```

To run the program in terminal mode run `tui.py` with root previleges 
```
sudo python3 tui.py
```

# Screenshots
![alt text](image.png)