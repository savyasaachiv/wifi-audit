from subprocess import Popen,run
from time import sleep

def scan(interface,debug=0):
    # CREATE A FUNCTION SCAN. THIS FUNCTION WILL BE RUN BY CALLING THE FILENAME(here in this case apscan).scan()
    # so the final syntax will be
    # apscan.scan()

    # running a system command, "run" function from the module subprocess.
    # the response we get from the run function that can be anything, a boolean value or a python object is stored
    # in tha variable "cmd".

    # running a system command `rm /tmp/apscan*`
    # rm is a command to delete files in linux and unix operating systems. deleting file that might have been left
    # out by the program when it was previously ran, to avoid any problems

    if not debug:
        cmd=run('rm /tmp/apscan*',shell=True,capture_output=True)

    # /dev/null is a special file in unix and linux operating systems. it can be thought of as a garbage dump.
    # we open this file to use with python
    devnull=open('/dev/null','w')


    # Popen is another function under the subprocess module. unlike subprocess.run, subprocess.Popen can start a 
    # system command and proceed to the next line of the program without having to wait for its termination 
    # unless the termination is specified or the command exits on itself after its completed execution

    cmd=Popen(f'airodump-ng {interface} -a -w /tmp/apscan --write-interval 1 --output-format csv',shell=True,stdout=devnull)
    # we run airodump-ng command, assign its returned value to variable named cmd. i am re-using the variable cmd
    # as the output of previous command is no longer useful to us. this will replace the old value with our new value
    # we got here
    
    # Popen takes various arguments, the three arguments i gave are
    # arg 1, the command that needs to be run.
    # arg 2, shell=True. Ignore this for now, or just think of it was important when running shell commands.
    # arg 3, stdout=devnull. set the standard output(stdout) to devnull. devnull is the file object we created before
    #        where all the data can be dumped.
    # we are redirecting the standard output to devnull because we dont want to print out all the commands on the 
    # terminal when running the function

    sleep(10)
    # we wait for the airodump-ng command to run for sometime. sleep(10) meaning sleep for 10 seconds. the program
    # will just wait for 10 seconds. 10s in not a mandatory value for airodump-ng. the reason we give it sometime is
    # so that the tool can capture at least some output.
    # reason why we used Popen and not run is
    # when calling run() it will start a shell process and wait for it to finish. the previous rm command wont take
    # much time and it will terminate after its done deleting the file
    # airodump-ng will run continuosly however and will not terminate until it recieves an interrupt signal(ctrl+c)
    # because we cannot mimic ctrl+c (afaik) we use the Popen function as it provides an option to terminate the
    # program, run does not have this feature.

    # we have terminated the program below
    cmd.terminate()

    # we also close the file object for devnull as it is no longer required
    devnull.close()

    aps={} # wifi mac key, bssid privacy cipher authen channel value
    # you should know what s a dictionary

    scanRes=open('/tmp/apscan-01.csv').read().split('\n\n')[0].strip().split('\n')[1:]
    # open the file where the output of airodump-ng is stored and run various string manipulation functions on it
    # lazy to explain each one of them, please do it yourself. you can check the file yourself and see how its
    # structured

    # for each line in the file split it with commas and add various values like channel privacy cipher to a list
    # and assign that list as the value to the key, which is the wifi mac address here
    # eg: { "wifi-mac": ["bssid","privacy","cipher","authentication","channel"] }

    for _ in scanRes:
        aa=_.split(',')
        if ' '!=aa[-2]:
            aps[aa[0].strip()]=[aa[-2].strip(),aa[5].strip(),aa[6].strip(),aa[7].strip(),aa[3].strip()]

    if not debug:
        cmd=run('rm /tmp/apscan*',shell=True,capture_output=True)

    return aps
    # return the values, so that the program that called it can get access to the values this function found
