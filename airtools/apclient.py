from subprocess import Popen, run
from time import sleep

def scan(interface, channel,bssid, debug=0):
    if not debug:
        run('rm /tmp/apclient*',shell=True)
    devnull=open('/dev/null','w')
    cmd=Popen(f'airodump-ng {interface} -c {channel} --bssid {bssid} -a -w /tmp/apclient --write-interval 1 --output-format csv',shell=True,stdout=devnull)
    sleep(10)

    cmd.terminate()
    devnull.close()

    clients=[]

    for _ in open('/tmp/apclient-01.csv').read().split('\n')[5:]:
        if _!='':
            clients.append(_.split(',')[0])

    if not debug:
        cmd=run('rm /tmp/apclient*',shell=True,capture_output=True)

    return clients
