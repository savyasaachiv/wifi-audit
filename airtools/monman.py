from subprocess import run

def monitor():
    cmd=run('airmon-ng check kill',shell=True,capture_output=True)
    if cmd.returncode!=0: return 0
    cmd=run('airmon-ng start wlan0',shell=True,capture_output=True)
    if cmd.returncode==0: return 1
    else: return 0

def managed():
    cmd=run('airmon-ng stop wlan0',shell=True,capture_output=True)
    if cmd.returncode==0: return 1
    else: return 0
